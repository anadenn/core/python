#!/usr/bin/python3
if __name__ == "__main__":
    from core.data import *
    from core.program import *
    #from core.models import *
    import console
    import sys,os
    from import_libs import import_all


    if __name__=="__main__":


        if len(sys.argv)<2:
            exit()

        path=os.environ["MAIN_PATH"]


        import_all(path)

        main=Main()
        Preload(parent=main,text=sys.argv[1])


        if len(sys.argv)<3:
            console.title("setup")
            console.add(main.setup())
            main.tree()
            console.title("cleanup")
            console.add(main.cleanup())

        else:
            main.start_point=sys.argv[2]
            console.add(main.run())

        console.show()
